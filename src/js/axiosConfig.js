let axiosConfig = {
  url: '',
  method: 'get',
  baseURL: 'https://localhost:7136',  //Se debe actualizar cambiar de host para la API
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  params: {},
  data: {}
};

export default axiosConfig;
