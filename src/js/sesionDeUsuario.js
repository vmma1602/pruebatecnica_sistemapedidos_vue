class SesionDeUsuario {
  #router;
  constructor(router) {
    this.#router = router;
  }

  comprobarSesionValida() {
    return localStorage.hasOwnProperty('usuario');
  }

  

  devolverDatosDeUsuario() {
    let datosUsuario = localStorage.getItem('usuario');
    return JSON.parse(datosUsuario);
  }

  redirigirUsuarioNoAutenticado() {
     this.#router.push('/login');
    return;
  }

  redirigirUsuarioNoAutorizado() {
     this.#router.push('/no-autorizado');
    return;
  }

  redirigirErrorDeServidor() {
    this.#router.push('/error-servidor');
   return;
 }

  comprobarTokenVigente(errorAxios) {
    //Falta comprobar el error de axios para ver si tiene
    // el header www-authenticate que diga que el token está expirado
  }

  verificarErrorHttp(codigoErrorHttp) {
    if (codigoErrorHttp === 401) {
      this.redirigirUsuarioNoAutenticado();
      localStorage.removeItem('usuario');
      return;
    }
    if (codigoErrorHttp === 403) {
      this.redirigirUsuarioNoAutorizado();
      return;
    }

    if (codigoErrorHttp >= 500 && codigoErrorHttp < 600) {
      this.redirigirErrorDeServidor();
      return;
    }
  }
}
export default SesionDeUsuario;
