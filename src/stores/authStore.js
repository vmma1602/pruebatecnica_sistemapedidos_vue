import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useAuthStore = defineStore('authStore', () => {
  let credenciales = ref({});

  function usuarioAutenticado() {
    return localStorage.hasOwnProperty('usuario');
  }

  function devolverDatosDeUsuario(){
    return JSON.parse(localStorage.getItem('usuario'));
  }

  return { credenciales, usuarioAutenticado, devolverDatosDeUsuario };
});
