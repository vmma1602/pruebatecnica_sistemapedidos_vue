import { createRouter, createWebHistory } from 'vue-router';
import { useAuthStore } from '../stores/authStore';

import Login from '../components/vistas/Login.vue';
import Inicio from '../components/vistas/Inicio.vue';
import ErrorDeServidor from '../components/errores/ErrorDeServidor.vue';
import NoAutorizado from '../components/errores/NoAutorizado.vue';
import NoEncontrado from '../components/errores/NoEncontrado.vue';
import Pedido from '../components/vistas/Pedido.vue';

const routes = [
  { name: 'login', path: '/login', component: Login },
  { name: 'noEncontrado', path: '/:pathMatch(.*)*', component: NoEncontrado },
  { name: 'errorDeServidor', path: '/error-servidor', component: ErrorDeServidor },
  { name: 'inicio', path: '/', component: Inicio },
  { name: 'pedidos', path: '/pedidos', component: Pedido },
  { name: 'noAutorizado', path: '/no-autorizado', component: NoAutorizado }
];

const history = createWebHistory();
const router = createRouter({
  history,
  routes
});

router.beforeEach(async (to) => {
  const paginasPublicas = ['/login'];
  const accesoRequerido = !paginasPublicas.includes(to.path);
  const authStore = useAuthStore();
  if (accesoRequerido && !authStore.usuarioAutenticado()) {
    //Requiere acceso y no está logueado
    return '/login';
  }

  if (authStore.usuarioAutenticado()) {
    authStore.credenciales = authStore.devolverDatosDeUsuario(); // Está logueado y cerró sesión
  }
});

export default router;
